<?php
require_once("db_inc.php");

?>
    <link href="style.css" rel="stylesheet" type="text/css">
<script src="script.js"></script>
<?php
$result = mysqli_query($link, "SELECT * FROM menu");
while ($row = mysqli_fetch_array($result)) {
    $data[$row['id']] = $row;
}

function view_cat($dataset)
{

    $arr = "";
    foreach ($dataset as $menu) {

        $arr .= " <li><a> " . $menu["name"] . " </a>";

        if (!empty($menu['childs'])) {
            $arr .= ' <div id="button" class="menu">
  <span class="title">Кнопка!</span> <ul>';
            $arr .= view_cat($menu['childs']);
            $arr .= '</ul> </div> ';
        }
        $arr .= '</li>';

    }
    return $arr;
}

function mapTree($dataset)
{

    $tree = array();

    foreach ($dataset as $id => &$node) {

        if (!$node['parent_id']) {
            $tree[$id] = &$node;
        } else {
            $dataset[$node['parent_id']]['childs'][$id] = &$node;
        }
    }
    return $tree;
}

$data = mapTree($data);

echo "<ul class='tree'>" . view_cat($data) . "</ul>";

?>

